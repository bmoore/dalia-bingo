BINGO
=====

| B | I | N | G | O |
| :-: | :-: | :-: | :-: | :-: |
| ~~Jennifer’s Wedding~~ | Mr. Larry         | ~~Bumble Rumble~~ | ~~It’s Pretty~~    | ~~Figurine~~          |
| ~~Harvey Koop~~        | ~~Tasagore~~          | ~~Jamaica~~       | ~~InspiRed~~  | ~~Hyzizzle~~          |
| ~~Insipic~~            | ~~Sorbet~~            | ~~FREE~~          | ~~Optic Illusion~~ | ~~Peaches and Cream~~ |
| ~~Coconut Puff~~       | ~~Colored Spectacle~~ | ~~White NW~~      | ~~Didley Squat (inspired freebee)~~   | ~~Little Bees Wings~~ |
| ~~Mr. Optimist~~       | ~~Fidelgo Climax~~    | Sakura Fabuki | Colorado’s Kid | ~~Hawaii~~            |

