import random
from tabulate import tabulate

dalias = ['Bumble Rumble','Bellred Desire','AC Dark Horse','Jamaica','Burg White Tips','Fidelgo Climax','Harvey Koop','Pineland’s Pam','Optic Illusion','Myrtle’s Folly','Penhill Dark Monarch','Colored Spectacle','Sorbet','Kagane Fabuki','Mr. Larry','InspiRed','Show N’Tell','Bloomquist Amethyst ','Tahoma Higgo','White NW','Popular Guest','Insipic','Figurine','Mr. Optimist','Evaline','Didley Squat','Chilson’s Pride','Hyzizzle','Coconut Puff','Caproz Jospehine','Hawaii','Versa','Top Honor','Alloway Candy','Alana Clair Obscura','Islander','It’s Pretty','Twynings Smartie','White Pom ','Bradley Aaron','Shea’s Rainbow','Peaches and Cream','Honka','Bristol’s Stripe','Little Bees Wings','Sakura Fabuki','Colorado’s Kid','Bright Star','Mars','Tasagore','Maggie C','Jennifer’s Wedding']

random.shuffle(dalias)

bingo = []
for i in range(5):
    i *= 5
    bingo.append(dalias[i:i+5])

bingo[2][2] = "FREE"

print(tabulate(bingo))
